# 0700 Intrduction à git



## Aide 

* [Documentation Git](https://git-scm.com/book/en/v2)

## Objectifs

* Créer un dépôt Git
* Effectuer un commit et un push
* Créer une branche
* Fusionner des branches

## Instructions 

### 1. Initialiser un dépôt Git

```
git init
```

### 2. Ajouter un fichier Python

Créez un fichier `hello_world.py` avec le code suivant :

```python
print("Hello World!")
```

### 3. Ajouter et commiter le fichier
Pour chaque étape, soyez attentif au résultat de la commande `git status`
```
git status
git add hello_world.py
git status
git commit -m "Ajout de hello_world.py"
git status
```

### 4. Créer une branche

```
git checkout -b feature
```

### 5. Modifier le script Python

Modifiez le message "Hello World!" dans le fichier `hello_world.py`.

### 6. Ajouter et commiter les modifications

```
git add hello_world.py
git commit -m "Modification du message Hello World"
```

### 7. Consulter  l'historique

```
git log
```

### 8. Revenir sur la branche principale

```
git checkout master
git log
```

### 9. Fusionner les deux branche

```
git merge feature
git log
```

### 10.  Créer un compte GitLab

- [Créez un compte gitlab](https://gitlab.com/users/sign_up), puis créez un dépôt **public** (Blank project)
- Dans le README.md, trouvez le lien du dépôt

### 11. Ajouter le dépôt distant au dépôt local

```
git remote add origin <lien du dépôt>
git push -uf origin main
```


### 12. Pousser les modifications vers le dépôt distant
```
git push origin feature
```

## Vérification :

* Assurez-vous que le fichier `hello_world.py` contient le message modifié.
* Vérifiez l'historique des commits pour voir les différentes modifications.

## Bonus :

* Essayez de créer un conflit de fusion en modifiant le même fichier dans les deux branches.
* Apprenez à résoudre les conflits de fusion.
